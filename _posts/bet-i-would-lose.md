title: A bet I'd love to lose
url: bet-i-would-lose.html
date: 2021-06-17
author: Rahul Jha
tags: r19a,politics
summary: Exactly what the title reads

## Abstract
The other day, Adnan <a class="footnote"><sup>1</sup><span> My best friend with too may girlfriends </span></a> predicted the outcomes of the next five elections. His predictions (without the profane commentary that accompanied them) were:
<ol>
<li>2024 : Narendra Modi </li>
<li>2029: <strike>Doraemon</strike> Amit Shah </li>
<li>2034: Amit Shah </li>
<li>2039: Yogi Adityanath </li>
</ol>
He wanted to go on, but I stopped him. Because if Yogi comes to power in 2039, Adnan has no business predicting any more elections. Why? Because Adnan is a muslim, and Yogi Adityanath is the closest thing India has to Taliban. And Taliban and minorities do not go together. Now, Adnan is usually very stupid<a class="footnote"><sup>2</sup><span>Case in point: he studies biology.</span></a> but his predictions this time are right on the money. And I wish they weren't.

## No unfounded beliefs
The liberals reading this blog (**die hard Congress supporters**) will say there's no way Modi is coming to power in 2024, let alone 2029. Well, what alternative does India have? I remember reading a novel written by Bill Clinton <a class = "footnote"><sup>3</sup><span>Yes, he wrote novels. Apparently Monica Lewinsky wasn't enough to keep him entertained.</span></a> in which the protagonist says: **"When you have many flat out shitty options, you choose one that is the least flat out shitty of them all"**, and that's how Narendra Modi and his band of cow-worshipping government-toppling men have been winning. Indian politics does not have a party that can stand up to BJP. Sure, Rahul Gandhi is very funny and my namesake<a class = "footnote"><sup>4</sup><span>Another example of Adnan's idiocy, he actually asked me why parents named me Rahul when they don't vote Congress</span></a> but has nothing that instills confidence among people. He speaks of atrocities committed during BJP's reign while choosing to forget the ones being committed in Congress ruled states. The regional stalwarts cannot speak chaste Hindi if their lives depended on it, and in order to win in the Lok Sabha elections, one needs to win the Hindi belt. Kejriwal is viable option- he is educated, suave and popular- but he takes fiscally imprudent decisions. The USP of Kejriwal's government in New Delhi is free electricity, and Kejriwal has promised free electricity in every election his party has contested. It takes little more than common sense to understand that India - with 1.35 billion people- cannot afford to give electricity for free to everyone without taxes so high that Indira Gandhi's taxation policies seem better<a class = "footnote"><sup>5</sup><span>The income tax during Indira Gandhi era was 103%, that's right.</span></a>.
All this, and a couple more reasons make me approve of Adnan's predictions. But here's why I want myself to be wrong.

## The Militant Monk
What do you do when you have a religious fanatic hell bent on beating the shit out of seculars and secularism<a class = "footnote"><sup>6</sup><span>He is hell bent on beating the shit out of lovers, too. But that does not concern me *sulk* *sulk*.</span></a>. The obvious answer is to avoid him, but alas it's too late for that. He already rules Uttar Pradesh and he has appeased the Hindus so much that there's no way he is being voted out. So the next best option is to restrict his sphere of atrocities. And once he becomes the prime minister, he is free to do whatever he wants. That's scary to say the least. And that's also why I'd rather be wrong this time.
